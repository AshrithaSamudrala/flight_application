package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Airline {
	
	@Id
	private String airlinedId;
	private String airlineName;
	public String getAirlinedId() {
		return airlinedId;
	}
	public void setAirlinedId(String airlinedId) {
		this.airlinedId = airlinedId;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	

}
