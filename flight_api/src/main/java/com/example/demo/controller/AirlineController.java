package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Airline;
import com.example.demo.model.Flight;
import com.example.demo.model.Inventory;
import com.example.demo.service.FlightService;

@RequestMapping("/api/v1.0/flight")
@RestController
public class AirlineController {
	
	@Autowired
	FlightService flightService;
	
	@PostMapping("/airline/register")
	public String createAirline(@RequestBody Airline airline)
	{
		flightService.saveAirlineDetails(airline);
	System.out.println("airline id : " +airline.getAirlinedId()+"\nairline name : "+airline.getAirlineName());
	return "New Airline Created";
	}

	@PostMapping("/airline/inventory/add")
	public String createInventory(@RequestBody Inventory inventory) {
		flightService.saveInventory(inventory);
		return "Inventory details have been added \n "+inventory.getAirline();
	}
	@PostMapping("/airline/flight")
	public void getFlight(@RequestBody Flight flight)
	{
		flightService.saveFlight(flight);
	}
	
	@PostMapping("/search")
	public List<Flight> searchFlight(@RequestBody Flight flight) {
		return flightService.getFlightDetails(flight);
		
		//return flight.getAirLineName()+"\n"+ flight.getAirLineLogo();
	}
}
