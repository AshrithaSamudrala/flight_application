package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Admin;
import com.example.demo.service.FlightService;

@RequestMapping("/api/v1.0/flight")
@RestController
public class AdminController {
	
	@Autowired
	FlightService flightService;
	
	@PostMapping("/admin/login")
	public String adminRegister(@RequestBody Admin admin) {
		flightService.saveAdmin(admin);
		return "admin details have been added successfully \n"+admin.getAdminId()
								+"\n admin name" +admin.getAdminName();
		
	}

}
