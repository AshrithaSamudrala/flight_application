package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Ticket;
import com.example.demo.service.FlightService;

@RequestMapping("/api/v1.0/flight")
@RestController
public class BookingController {
	
	@Autowired
	FlightService flightService;
	
	@PostMapping("/booking/{flightNumber}")
	public String BookTicket(@RequestBody Ticket ticket) {
		ticket.setPnr((int)Math.floor(Math.random() * 10) + 1);
		System.out.println(ticket.getPnr());
		flightService.saveTicket(ticket);
		return "";
		 
		 //return ticket.getUserName() +"\n" +ticket.getPassengerName();
	}
	
	@GetMapping("/ticket/{pnr}")
	public Ticket TicketDetails(@PathVariable int pnr) {
		return flightService.getByPnr(pnr);
		
	}
	
	@GetMapping("/booking/history/{emailID}")
	public Ticket TicketByEmail(@PathVariable String emailID) {
		return flightService.getByEmailId(emailID);
		
	}
	
	@DeleteMapping("/booking/cancel/{pnr}")
	public void DeleteByPnr(@PathVariable int pnr) {
		flightService.deleteByPnr(pnr);
	}
}
