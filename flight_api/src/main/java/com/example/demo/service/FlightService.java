package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Repository.AdminRepository;
import com.example.demo.Repository.AirlineRepository;
import com.example.demo.Repository.FlightRepository;
import com.example.demo.Repository.InventoryRepository;
import com.example.demo.Repository.TicketRepository;
import com.example.demo.model.Admin;
import com.example.demo.model.Airline;
import com.example.demo.model.Flight;
import com.example.demo.model.Inventory;
import com.example.demo.model.Ticket;

@Service
public class FlightService {
	@Autowired
	InventoryRepository inventoryRepository;
	
	@Autowired
	TicketRepository ticketRepository;
	
	@Autowired
	FlightRepository flightRepository;
	
	@Autowired
	AirlineRepository airlinerepository;
	
	@Autowired
	AdminRepository adminRepository;

	public void saveAirlineDetails(Airline airline) {
		airlinerepository.save(airline);	
	}

	public Inventory saveInventory(Inventory inventory) {

		return inventoryRepository.save(inventory);
	}

	public void saveFlight(Flight flight) {
		flightRepository.save(flight);
	}

	public void saveAdmin(Admin admin) {
		adminRepository.save(admin);
		
	}

	public List<Flight> getFlightDetails(Flight flightsent) {
		List<Flight> flightList= new ArrayList<Flight>();
		List<Flight> flightRepo =flightRepository.findAll();
		for(Flight f:flightRepo)
		{
			if(f.getDate().contentEquals(flightsent.getDate()) && (f.getTime().contentEquals(flightsent.getTime()) &&
			(f.getFromPlace().contentEquals(flightsent.getFromPlace()) && (f.getToPlace().contentEquals(flightsent.getToPlace()))  )))
			{
				flightList.add(f);
			}
		}
		return flightList;
	}

	public void saveTicket(Ticket ticket) {
		ticketRepository.save(ticket);
		
	}

	public Ticket getByPnr(int pnr) {

		List<Ticket> ticketlist = ticketRepository.findAll();
		for(Ticket t :ticketlist) {
			if(t.getPnr()==pnr)
				return t;
		}
		return null;
	}

	public Ticket getByEmailId(String emailID) {
		List<Ticket> ticketlists = ticketRepository.findAll();
		for(Ticket k :ticketlists) {
			if(k.getEmailID().contentEquals(emailID))
				return k;
		}
		return null;
	}

	public void deleteByPnr(int pnr) {
		List<Ticket> ticketlist = ticketRepository.findAll();
		for(Ticket t :ticketlist) {
			if(t.getPnr()==pnr)
				ticketRepository.delete(t);
		}
		
	}
	
}
