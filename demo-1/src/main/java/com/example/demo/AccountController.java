package com.example.demo;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/account")
@RestController
public class AccountController {

	
	@GetMapping("/get/{id}/{name}")
	public String call(@PathVariable Integer id, @PathVariable String name) {
		System.out.println("Id of the user "+id+"\nName of the user : "+name);
		return "id and name has been successfully printed \n ID : " +id +"\n Name : " +name;
	}
	
	ArrayList<Account> list = new ArrayList<>();
	
	@PostMapping("/post")
	private String call(@RequestBody Account account) {
		System.out.println("Details of the account holder:\n name: "+account.getName()
								+"\n Id : "+account.getId()+ "\n Amount : "+account.getAmount());
		list.add(account);
		return "details have been received";
	}
	@GetMapping("/retrieve")
	public ArrayList<Account> retrieveDetails() {
		return list;
		
	}

}
