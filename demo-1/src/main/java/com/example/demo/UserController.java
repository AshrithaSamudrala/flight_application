package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	@GetMapping("/test")
	void call() {
		System.out.println("called");
	}
	@GetMapping("/{id}")
	void callers(@PathVariable Integer id) {
		System.out.println("called "+id);
	}
	@PostMapping
	private String saveUser(@RequestBody User user) {
		userService.save(user);
		System.out.println("post method user details "+user.getName());
		return "success";
	}
	@PutMapping("/put")
	public String putCall() {
		System.out.println("Put method");
		return "put method success";
	}
}
